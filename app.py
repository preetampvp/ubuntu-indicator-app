from gi.repository import Gtk as gtk
from gi.repository import AppIndicator3 as ai
from gi.repository import Notify as notify
import os


APP_INDICATOR_ID = 'app'

def main():
    indicator = ai.Indicator.new(APP_INDICATOR_ID, os.path.abspath('icon.svg'), ai.IndicatorCategory.SYSTEM_SERVICES)
    indicator.set_status(ai.IndicatorStatus.ACTIVE)
    indicator.set_menu(build_menu())
    notify.init(APP_INDICATOR_ID)
    gtk.main()

def build_menu():
    menu =  gtk.Menu()

    item_notification = gtk.MenuItem("Show notification")
    item_notification.connect('activate', show_notification)
    menu.append(item_notification)

    item_quit = gtk.MenuItem("Quit")
    item_quit.connect('activate', quit)
    menu.append(item_quit)

    menu.show_all()
    return menu

def quit(source):
    notify.uninit()
    gtk.main_quit()

def show_notification(source):
    notify.Notification.new("Hello", "From the other side", None).show()

if __name__ == "__main__":
    main()
